﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Constants
{
    public class CookiesConstants
    {
        public const string CookieScheme = "Cookie.LeaveManagement";
        public const string CookieName = "LeaveManagement.Cookie";
        public const string LoginPath = "/pools/home";
        public const string AccessDeniedPath = "/pools/accessdenied";
    }
}
