﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Constants
{
    public class PoliciesConstants
    {
        public const string BearerPolicy = "Bearer";
        public const string UserPolicy = "UserPolicy";
        public const string HostPolicy = "HostPolicy";
        public const string AdminPolicy = "AdminPolicy";
    }
}
