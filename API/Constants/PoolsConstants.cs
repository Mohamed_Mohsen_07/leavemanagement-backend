﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Constants
{
    public class PoolsConstants
    {
        //public const string Upload = "\\Uploads\\";
        private const string SrcRootPath = @"C:\Workspace\LeaveManagement\Front-End\src\";
        private const string AssetsFolder = @"assets\";
        public const string PoolsImagesRelativeUploadingFolder = AssetsFolder +  @"images\Pools\";
        public const string PoolsImagesAbsoluteUploadingFolder = SrcRootPath + AssetsFolder +  @"images\Pools\";
        public const int DefaultPageSize = 10;
    }
}
