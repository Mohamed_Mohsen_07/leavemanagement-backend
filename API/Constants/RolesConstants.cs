﻿namespace API.Constants
{
    public class RolesConstants
    {
        public const string User = "user";
        public const string Admin = "admin";
    }
}
