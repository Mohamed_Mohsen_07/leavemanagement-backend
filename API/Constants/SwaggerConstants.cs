﻿namespace API.Constants
{
    // https://localhost:44394/swagger/index.html
    public class SwaggerConstants
    {
        public const string DocVersion = "v1";
        public const string DocName = "v1";
        public const string DocTitle = "LeaveManagement APIs";
        public const string Endpoint = "/swagger/v1/swagger.json";
        public const string EndpointName = "LeaveManagement APIs version 1";
    }
}
