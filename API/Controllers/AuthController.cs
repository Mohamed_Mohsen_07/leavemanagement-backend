﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Constants;
using AutoMapper;
using BLL.DTOs;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResponseHandler.Factories.Interfaces;
using ResponseHandler.Enums;
using System.Collections.Generic;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IResponseFactory _responseFactory;
        private readonly IJWTTokenGeneratorService _jWTTokenGeneratorService;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ICurrentUserService _currentUserService;
        private readonly IErrorConverterService _errorConverterService;
        private readonly IUserManagerService _userManagerService;

        public AuthController(
            IUserManagerService userManagerService,
            IErrorConverterService errorConverterService,
            ICurrentUserService currentUserService,
            IMapper mapper, 
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IResponseFactory responseFactory,
            IJWTTokenGeneratorService jWTTokenGeneratorService,
            RoleManager<IdentityRole> roleManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _responseFactory = responseFactory;
            _jWTTokenGeneratorService = jWTTokenGeneratorService;
            _roleManager = roleManager;
            _currentUserService = currentUserService;
            _errorConverterService = errorConverterService;
            _userManagerService = userManagerService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginDto accountLoginDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(_responseFactory.GenerateAlertMessages(Code.ModelStateNotValid, _errorConverterService.ConvertModelStateErrorsToListOfErrorMessages(ModelState), ModelState.Values));
                }

                ApplicationUser userFromDb = null;
                Code code = Code.NONE;

                if (accountLoginDto.IsEmailLoginBased())
                {
                    userFromDb = await _userManager.FindByEmailAsync(accountLoginDto.UserNameOrEmail);
                    code = Code.IncorrectEmailOrPassword;
                }
                else
                {
                    userFromDb = await _userManager.FindByNameAsync(accountLoginDto.UserNameOrEmail);
                    code = Code.IncorrectUserNameOrPassword;
                }

                if (userFromDb == null)
                {
                    return Ok(_responseFactory.Generate(code));
                }

                //var result = await _signInManager.CheckPasswordSignInAsync(userFromDb, accountLoginDto.Password, false);
                var result = await _signInManager.PasswordSignInAsync(userFromDb, accountLoginDto.Password, accountLoginDto.RememberMe, false);

                if (!result.Succeeded)
                {
                    return Ok(_responseFactory.Generate(code));
                }

                var userLoginResult = _mapper.Map<UserLoginResultDto>(userFromDb);
                var roles = await _userManager.GetRolesAsync(userFromDb);
                var claims = await _userManager.GetClaimsAsync(userFromDb);

                userLoginResult.Token = _jWTTokenGeneratorService.GenerateToken(userFromDb, roles, claims);


                return Ok(_responseFactory.Generate(Code.SuccessLogin, userLoginResult, new[] { userLoginResult.UserName }));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.LoginException, ex));
            }
        }

        [HttpPost]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok(_responseFactory.Generate(Code.SuccessLogout, true));
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterDto accountRegisterDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Ok(_responseFactory.GenerateAlertMessages(Code.ModelStateNotValid, _errorConverterService.ConvertModelStateErrorsToListOfErrorMessages(ModelState), ModelState.Values));
                }                

                var userToCreate = _mapper.Map<ApplicationUser>(accountRegisterDto);
                var result = await _userManager.CreateAsync(userToCreate, accountRegisterDto.Password);

                if (!result.Succeeded)
                {
                    return Ok(_responseFactory.GenerateAlertMessages(Code.FailedToCreateNewUser, _userManagerService.ConvertResultToListOfMessages(result), result));
                }

                var userFromDb = await _userManager.FindByNameAsync(userToCreate.UserName);
                //await _userManager.AddClaimAsync(userFromDb, new Claim(ClaimTypes.NameIdentifier, userFromDb.Id));
                //await _userManager.AddClaimAsync(userFromDb, new Claim(ClaimTypes.Name, userFromDb.UserName));
                //await _userManager.AddClaimAsync(userFromDb, new Claim(ClaimTypes.Email, userFromDb.Email));
                await _userManager.AddToRoleAsync(userFromDb, RolesConstants.User);

                return Ok(_responseFactory.Generate(Code.SuccessCreateNewUser, result));
            }
            catch(Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.RegisterException, ex));
            }
        }

        [HttpGet]
        [Route("is-logged-in")]
        [AllowAnonymous]
        public IActionResult IsLoggedIn()
        {
            bool isUserLoggedIn = this._currentUserService.IsLoggedIn();
            if (!isUserLoggedIn)
            {
                return Ok(_responseFactory.Generate(Code.FailedIsLoggedIn, isUserLoggedIn));
            }
            return Ok(_responseFactory.Generate(Code.SuccessIsLoggedIn, isUserLoggedIn));
        }

    }
}