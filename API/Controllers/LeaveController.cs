﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Constants;
using BLL.DTOs;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ResponseHandler.Factories.Interfaces;
using ResponseHandler.Enums;


namespace API.Controllers
{
    [Route("api/leaves")]
    [ApiController]
    [Authorize]
    public class LeaveController : ControllerBase
    {

        private readonly ILeaveService _leaveService;
        private readonly IResponseFactory _responseFactory;
        private readonly ICurrentUserService _currentUserService;

        public LeaveController(ICurrentUserService currentUserService, ILeaveService leaveService, IResponseFactory responseFactory)
        {
            _leaveService = leaveService;
            _responseFactory = responseFactory;
            _currentUserService = currentUserService;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IActionResult> GetAllLeaves()
        {
            try
            {
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, await _leaveService.GetAllAsync()));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }

        [Route("user-leaves")]
        [HttpGet]
        public async Task<IActionResult> GetUserLeaves(Guid UserId)
        {
            try
            {
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, await _leaveService.GetAllForUserAsync(UserId)));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(LeaveDto leaveDto)
        {
            try
            {
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, await _leaveService.CreateAsync(leaveDto)));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }

    }
}
