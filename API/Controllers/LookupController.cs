﻿using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ResponseHandler.Factories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResponseHandler.Enums;

namespace API.Controllers
{
    [Route("api/lookups")]
    [ApiController]
    public class LookupController : ControllerBase
    {
        private readonly ILookupService _lookupService;
        private readonly IResponseFactory _responseFactory;

        public LookupController(ILookupService lookupService, IResponseFactory responseFactory)
        {
            _lookupService = lookupService;
            _responseFactory = responseFactory;
        }

        [Route("leave-reasons")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetLeaveReasons()
        {
            try
            {
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, await _lookupService.GetLeaveReasons()));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }
    }
}
