﻿using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ResponseHandler.Factories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ResponseHandler.Enums;
using BLL.DTOs;
using Microsoft.AspNetCore.Http;

namespace API.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IApplicationUserService _applicationUserService;
        private readonly IResponseFactory _responseFactory;
        private readonly ICurrentUserService _currentUserService;

        public UserController(ICurrentUserService currentUserService, IApplicationUserService applicationUserService, IResponseFactory responseFactory)
        {
            _applicationUserService = applicationUserService;
            _responseFactory = responseFactory;
            _currentUserService = currentUserService;
        }


        [Route("profile")]
        [HttpGet]
        public async Task<IActionResult> GetProfileInfo(Guid UserId)
        {
            try
            {
                if(this._currentUserService.GetUserId() != UserId)
                {
                    return Ok(_responseFactory.Generate(Code.FailedToRetriveData));
                }
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, await _applicationUserService.GetUserProfileDetails(UserId)));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }

        [Route("update-profile")]
        [HttpPut]
        public IActionResult UpdateGetProfileInfo(UserProfileDto userUpdatedProfile)
        {
            try
            {
                // Make sure that the logged in user who is the person that tries to updting his/her profile info.
                if (this._currentUserService.GetUserId() != userUpdatedProfile.Id)
                {
                    return Ok(_responseFactory.Generate(Code.FailedToRetriveData));
                }
                return Ok(_responseFactory.Generate(Code.SuccessRetriveData, _applicationUserService.UpdateUserProfileDetails(userUpdatedProfile)));
            }
            catch (Exception ex)
            {
                return Ok(_responseFactory.Generate(Code.FailedToRetriveData, ex));
            }
        }
    }
}
