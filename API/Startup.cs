using AutoMapper;
using BLL.Services.Implementations;
using BLL.Services.Interfaces;
using DAL.Data;
using DAL.Models;
using DAL.Repositories.Implementations;
using DAL.Repositories.Interfaces;
using DAL.UnitOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using System.Text;
using System;
using Microsoft.AspNetCore.Authorization;
using API.Constants;
using Microsoft.OpenApi.Models;
using ResponseHandler.Factories.Interfaces;
using ResponseHandler.Factories.Implementations;
using BLL.Constants;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // Services lifetime: https://www.c-sharpcorner.com/article/service-lifetime-dependency-injection-asp-net-core/
        public void ConfigureServices(IServiceCollection services)
        {
            #region Register Controllers

            services.AddControllers();

            #endregion

            #region Register DbContext

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            #endregion

            #region Register Services

            services.AddScoped<IJWTTokenGeneratorService, JWTTokenGeneratorService>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<ILeaveRepository, LeaveRepository>();
            services.AddScoped<ILeaveReasonRepository, LeaveReasonRepository>();
            services.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            services.AddScoped<IApplicationUnitOfWork, ApplicationUnitOfWork>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddTransient<ClaimsPrincipal>(s => s.GetService<IHttpContextAccessor>().HttpContext.User);
            services.AddTransient<ILeaveService, LeaveService>();
            services.AddTransient<ILookupService, LookupService>();
            services.AddTransient<IApplicationUserService, ApplicationUserService>();
            services.AddSingleton<IResponseFactory, ResponseFactory>();
            services.AddSingleton<IErrorConverterService, ErrorConverterService>();
            services.AddSingleton<IUserManagerService, UserManagerService>();

            #endregion

            #region Register AutoMapper Profiles

            //If you have other mapping profiles defined, those profiles will be loaded too automcatically.
            services.AddAutoMapper(Assembly.GetAssembly(typeof(BLL.AutoMapper.Profiles.ApplicationUserProfile)));

            #endregion

            #region Register Identity

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
                {
                    config.SignIn.RequireConfirmedEmail = false;
                    config.User.RequireUniqueEmail = true;
                    config.User.AllowedUserNameCharacters = AppIdentityConstants.AllowedUserNameCharacters;
                    config.Password.RequiredLength = 8;
                    config.Password.RequireDigit = true;
                    config.Password.RequireLowercase = true;
                    config.Password.RequireUppercase = true;
                    config.Password.RequireNonAlphanumeric = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>();

            #endregion

            #region Register JWT

            services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                config.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                //This should turned to off when it comes to live.
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration[TokenConstants.ConfigKey])),
                    ValidIssuer = Configuration[TokenConstants.ConfigIssuer],
                    ValidAudience = Configuration[TokenConstants.ConfigAudience],
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddAuthorization(config =>
            {
                config.AddPolicy(PoliciesConstants.BearerPolicy, new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser()
                        .Build());

                config.AddPolicy(PoliciesConstants.UserPolicy, options =>
                {
                    //options.RequireClaim(Claims.FullNameClaim);
                    options.RequireRole(new[] {
                        RolesConstants.User,
                        RolesConstants.Admin });
                });

                config.AddPolicy(PoliciesConstants.AdminPolicy, options =>
                {
                    //options.RequireClaim(Claims.FullNameClaim);
                    options.RequireRole(new[] {
                        RolesConstants.Admin });
                });
            });

            #endregion

            #region Register Swagger

            services.AddSwaggerGen(x =>
            {

                x.SwaggerDoc(SwaggerConstants.DocName, new OpenApiInfo()
                {
                    Title = SwaggerConstants.DocTitle,
                    Version = SwaggerConstants.DocVersion,
                });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer" , new string[0] }
                };
                x.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    Description = "Jwt Authorization header using the bearer shceme",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                x.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                {
                new OpenApiSecurityScheme
                {
            Reference = new OpenApiReference
              {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
              },
              Scheme = "oauth2",
              Name = "Bearer",
              In = ParameterLocation.Header,

            },
            new List<string>()
          }
        });

            });


            #endregion

            #region Register Cors Origins

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.WithOrigins("https://localhost:4200"));
            });

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options => {
                options.SwaggerEndpoint(SwaggerConstants.Endpoint, SwaggerConstants.EndpointName);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseRouting();

            // Who are you?
            app.UseAuthentication();
            
            // Do you allowed?
            app.UseAuthorization();

            app.UseCookiePolicy();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization(PoliciesConstants.UserPolicy);
            });
        }
    }
}