﻿using AutoMapper;
using BLL.DTOs;
using DAL.Models;

namespace BLL.AutoMapper.Profiles
{
    public class ApplicationUserProfile: Profile
    {
        public ApplicationUserProfile()
        {
            CreateMap<UserRegisterDto, ApplicationUser>();
            CreateMap<ApplicationUser, UserLoginResultDto>();  
            CreateMap<ApplicationUser, UserProfileDto>().ReverseMap();
            CreateMap<UserProfileDto, ApplicationUser>().ReverseMap();

        }

        public override string ProfileName
        {
            get { return "ApplicationUserProfile"; }
        }
    }
}
