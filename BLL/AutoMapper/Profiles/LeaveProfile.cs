﻿using AutoMapper;
using BLL.DTOs;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.AutoMapper.Profiles
{
    public class LeaveProfile : Profile
    {
        public LeaveProfile()
        {
            CreateMap<LeaveReasonDto, LeaveReason>().ReverseMap();
            CreateMap<LeaveReason, LeaveReasonDto>().ReverseMap();
            CreateMap<Leave, LeaveDto>().ReverseMap();
            CreateMap<LeaveDto, Leave>().ReverseMap();
            CreateMap<LeaveResultDto, Leave>().ReverseMap();
            CreateMap<Leave, LeaveResultDto>().ReverseMap();
        }

        public override string ProfileName
        {
            get { return "LeaveProfile"; }
        }
    }
}
