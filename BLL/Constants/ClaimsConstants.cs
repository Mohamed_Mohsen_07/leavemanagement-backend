﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Constants
{
    public class ClaimsConstants
    {
        public const string UserName = "userName";
        public const string Email = "email";
        public const string UserIdentifier = "userIdentifier";
        public const string Role = "role";
    }
}
