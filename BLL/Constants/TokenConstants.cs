﻿namespace BLL.Constants
{
    public class TokenConstants
    {
        public const string ConfigKey = "Token:Key";
        public const string ConfigIssuer = "Token:Issuer";
        public const string ConfigAudience = "Token:Audience";
        public const int ExpirationInMinutes = 180;
    }
}
