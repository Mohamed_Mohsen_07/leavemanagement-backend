﻿using DAL.Models;
using System;

namespace BLL.DTOs
{
    public class LeaveDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Details { get; set; }
        public int LeaveReasonId { get; set; }
        public string UserId { get; set; }
    }
}
