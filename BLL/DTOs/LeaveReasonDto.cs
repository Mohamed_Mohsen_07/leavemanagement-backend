﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class LeaveReasonDto
    { 
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
