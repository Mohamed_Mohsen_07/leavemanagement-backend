﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class LeaveResultDto
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Details { get; set; }
        public LeaveReasonDto Reason { get; set; }
    }
}
