﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace BLL.DTOs
{
    public class UserLoginDto
    {
        [Required]
        public string UserNameOrEmail { get; set; }        
        [Required]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        /// <summary>
        /// Is user try to login using his/her email.
        /// </summary>
        /// <returns>True if the entered text contains @ special character.</returns>
        public bool IsEmailLoginBased() => UserNameOrEmail.Contains('@');
    }
}
