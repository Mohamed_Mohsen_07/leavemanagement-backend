﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.DTOs
{
    public class UserLoginResultDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }        
    }
}
