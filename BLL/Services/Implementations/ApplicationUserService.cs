﻿using AutoMapper;
using BLL.DTOs;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Implementations
{
    public class ApplicationUserService : IApplicationUserService
    {
        private readonly IApplicationUnitOfWork _uow;
        private readonly IMapper _mapper;

        public ApplicationUserService(IApplicationUnitOfWork applicationUnitOfWork, IMapper mapper)
        {
            _uow = applicationUnitOfWork;
            _mapper = mapper;
        }

        public async Task<UserProfileDto> GetUserProfileDetails(Guid userId)
        {
            ApplicationUser user = await _uow.ApplicationUserRepository.GetAsync(userId.ToString());
            UserProfileDto userProfile = _mapper.Map<UserProfileDto>(user);
            return userProfile;
        }
        
        public UserProfileDto UpdateUserProfileDetails(UserProfileDto userUpdatedProfile)
        {
            ApplicationUser updatedUserProfileData = _mapper.Map<ApplicationUser>(userUpdatedProfile);
            _uow.ApplicationUserRepository.Update(updatedUserProfileData);
            return userUpdatedProfile;
        }
    }
}
