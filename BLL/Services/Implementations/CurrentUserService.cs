﻿using BLL.Services.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace BLL.Services.Implementations
{
    public class CurrentUserService : ICurrentUserService
    {
        private ClaimsPrincipal _user;
        public CurrentUserService(ClaimsPrincipal user)
        {
            this._user = user;
        }

        public Guid GetUserId()
        {
            Guid userId;
            string _userId = this._user.FindFirstValue(ClaimTypes.NameIdentifier);
            if (string.IsNullOrEmpty(_userId) || !Guid.TryParse(_userId, out userId))
            {
                return new Guid();
            }
            return userId;
        }

        public ClaimsPrincipal GetUser()
        {
            return this._user;
        }

        public string GetUserEmail()
        {
            string email = this._user.FindFirstValue(ClaimTypes.NameIdentifier);
            if (string.IsNullOrEmpty(email))
            {
                return string.Empty;
            }
            return email;
        } 
        
        public bool IsLoggedIn()
        {
            return this._user.Identity.IsAuthenticated;
        }

        public void LogOut()
        {
            
        }

        public string GetUserName()
        {
            throw new NotImplementedException();
        }
    }
}
