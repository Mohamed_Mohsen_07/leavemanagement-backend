﻿using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services.Implementations
{
    public class ErrorConverterService : IErrorConverterService
    {
        public List<string> ConvertModelStateErrorsToListOfErrorMessages(ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage)).ToList();
        }
    }
}