﻿using Microsoft.Extensions.Configuration;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BLL.Constants;

namespace BLL.Services.Implementations
{
    public class JWTTokenGeneratorService : IJWTTokenGeneratorService
    {
        private readonly IConfiguration _config;
        public JWTTokenGeneratorService(IConfiguration config)
        {
            _config = config;
        }
        public string GenerateToken(ApplicationUser user, IList<string> roles, IList<Claim> claims)
        {
            if (claims == null)
            {
                claims = new List<Claim>();
            }

            claims.Add(new Claim(ClaimsConstants.UserIdentifier, user.Id));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));
            claims.Add(new Claim(ClaimsConstants.UserName, user.UserName));
            claims.Add(new Claim(ClaimsConstants.Email, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Email));
            claims.Add(new Claim(JwtRegisteredClaimNames.Nbf, DateTime.Now.ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimsConstants.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config[TokenConstants.ConfigKey]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var Token = new JwtSecurityToken(
                issuer: _config[TokenConstants.ConfigIssuer],
                audience: _config[TokenConstants.ConfigAudience],
                expires: DateTime.Now.AddMinutes(TokenConstants.ExpirationInMinutes),
                claims: claims,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(Token);
        }
    }
}
