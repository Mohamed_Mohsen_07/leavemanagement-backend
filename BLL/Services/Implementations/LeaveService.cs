﻿using AutoMapper;
using BLL.AutoMapper.Profiles;
using BLL.Constants;
using BLL.DTOs;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.Repositories.Implementations;
using DAL.Repositories.Interfaces;
using DAL.UnitOfWork;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace BLL.Services.Implementations
{
    public class LeaveService : ILeaveService
    {
        private readonly IApplicationUnitOfWork _uow;
        private readonly IMapper _mapper;
        public LeaveService(IApplicationUnitOfWork applicationUnitOfWork, IMapper mapper)
        {
            _uow = applicationUnitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LeaveDto>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<LeaveDto>>(await _uow.LeaveRepository.GetAllAsync());
        }

        public async Task<IEnumerable<LeaveDto>> GetByIdAsync(int leaveId)
        {
            return _mapper.Map<IEnumerable<LeaveDto>>(await _uow.LeaveRepository.GetAsync(leaveId));
        }

        public async Task<IEnumerable<LeaveDto>> GetAllForUserAsync(Guid userId)
        {
            return _mapper.Map<IEnumerable<LeaveDto>>(await _uow.LeaveRepository.WhereAsync(leave => leave.UserId == userId));
        }

        public async Task<LeaveResultDto> CreateAsync(LeaveDto leaveDto)
        {
            // NOT COMPLETED. WILL THROW AN ERROR.
            Leave leave = _mapper.Map<Leave>(leaveDto);
            leave.User = await _uow.ApplicationUserRepository.GetAsync(leaveDto.UserId);
            await _uow.LeaveRepository.AddAsync(leave);
            _uow.Commit();
            LeaveResultDto addedleave = _mapper.Map<LeaveResultDto>(leave);
            return addedleave;
        }
    }
}
