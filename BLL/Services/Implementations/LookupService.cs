﻿using AutoMapper;
using BLL.DTOs;
using BLL.Services.Interfaces;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Implementations
{
    public class LookupService : ILookupService
    {
        private readonly IApplicationUnitOfWork _uow;
        private readonly IMapper _mapper;

        public LookupService(IApplicationUnitOfWork applicationUnitOfWork, IMapper mapper)
        {
            _uow = applicationUnitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<LeaveReasonDto>> GetLeaveReasons()
        {
            return _mapper.Map<IEnumerable<LeaveReasonDto>>(await _uow.LeaveReasonRepository.GetAllAsync());
        }

    }
}
