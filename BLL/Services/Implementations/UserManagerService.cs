﻿using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services.Implementations
{
    public class UserManagerService: IUserManagerService
    {
        public List<string> ConvertResultToListOfMessages(IdentityResult result)
        {
            List<string> errors = new List<string>();
            foreach (var error in result.Errors)
            {
                errors.Add(error.Description);
            }
            return errors;
        }
    }
}
