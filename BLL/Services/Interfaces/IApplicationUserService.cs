﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IApplicationUserService
    {
        Task<UserProfileDto> GetUserProfileDetails(Guid userId);
        public UserProfileDto UpdateUserProfileDetails(UserProfileDto userUpdatedProfile);
    }
}
