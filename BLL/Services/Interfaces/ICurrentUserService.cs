﻿using System;
using System.Security.Claims;

namespace BLL.Services.Interfaces
{
    public interface ICurrentUserService
    {
        Guid GetUserId();
        string GetUserName();
        string GetUserEmail();
        bool IsLoggedIn();
        ClaimsPrincipal GetUser();
    }
}
