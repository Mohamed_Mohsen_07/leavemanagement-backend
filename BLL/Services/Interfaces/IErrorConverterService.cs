﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services.Interfaces
{
    public interface IErrorConverterService
    {
        List<string> ConvertModelStateErrorsToListOfErrorMessages(ModelStateDictionary ModelState);
    }
}
