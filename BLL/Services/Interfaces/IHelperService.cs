﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services.Interfaces
{
    public interface IHelperService
    {
        void SaveImage(string DirectoryPath, IFormFile Image);
    }
}
