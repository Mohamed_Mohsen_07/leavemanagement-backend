﻿using DAL.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace BLL.Services.Interfaces
{
    public interface IJWTTokenGeneratorService
    {
        string GenerateToken(ApplicationUser user, IList<string> roles, IList<Claim> claims);
    }
}
