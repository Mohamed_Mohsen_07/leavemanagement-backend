﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services.Interfaces
{
    public interface IUserManagerService
    {
        List<string> ConvertResultToListOfMessages(IdentityResult Result);
    }
}
