﻿using BLL.DTOs;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface ILeaveService
    {
        Task<IEnumerable<LeaveDto>> GetAllAsync();
        Task<LeaveResultDto> CreateAsync(LeaveDto leaveDto);
        Task<IEnumerable<LeaveDto>> GetByIdAsync(int leaveId);
        Task<IEnumerable<LeaveDto>> GetAllForUserAsync(Guid userId);
    }
}
