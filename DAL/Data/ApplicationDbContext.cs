﻿using DAL.ModelBuilders;
using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Main Application DbContext
        /// </summary>
        /// <param name="options">options to configure the DbContext such as changing the Data Source.</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Leave> Leaves{ get; set; }
        public DbSet<LeaveReason> LeaveReasons { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new IdentityRoleModelBuilderConfiguration());

            builder.Entity<Leave>().HasQueryFilter(leave => EF.Property<bool>(leave, "IsDeleted") == false);

        }
    }
}
