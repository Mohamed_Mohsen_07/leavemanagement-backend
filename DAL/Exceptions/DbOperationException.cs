﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Exceptions
{
    [Serializable()]
    public class DbOperationException: Exception
    {
        public DbOperationException() : base() { }
        public DbOperationException(string message) : base(message) { }
        public DbOperationException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected DbOperationException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
