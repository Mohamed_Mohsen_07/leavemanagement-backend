﻿using System.Collections.Generic;

namespace DAL.Models
{
    public class ApplicationRole : BaseModel
    {
        public string NameEn { get; set; }
        public string DescriptionEn { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
