﻿using System;
using Microsoft.AspNetCore.Identity;

namespace DAL.Models
{
    public class ApplicationUser: IdentityUser, IBaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ZipCode { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Bio { get; set; }
        public bool IsDeleted { get; set; }
    }
}
