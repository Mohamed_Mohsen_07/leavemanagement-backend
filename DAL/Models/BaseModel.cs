﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class BaseModel : IBaseModel
    {
        [Key]
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }

    public interface IBaseModel
    {
        public bool IsDeleted { get; set; }
    }
}
