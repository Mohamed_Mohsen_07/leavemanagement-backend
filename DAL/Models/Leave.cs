﻿using System;

namespace DAL.Models
{
    public class Leave : BaseModel
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Details { get; set; }
        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int LeaveReasonId { get; set; }
        public LeaveReason LeaveReason { get; set; }
    }
}
