﻿namespace DAL.Models
{
    public class LeaveReason : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
