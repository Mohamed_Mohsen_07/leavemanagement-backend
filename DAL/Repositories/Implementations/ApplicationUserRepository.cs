﻿using DAL.Data;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Implementations
{
    public class ApplicationUserRepository: Repository<ApplicationUser> , IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext context): base(context)
        {
        }
    }
}
