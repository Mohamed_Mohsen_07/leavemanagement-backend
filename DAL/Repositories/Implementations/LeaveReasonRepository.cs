﻿using DAL.Data;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Implementations
{
    public class LeaveReasonRepository : Repository<LeaveReason>, ILeaveReasonRepository
    {
        public LeaveReasonRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
