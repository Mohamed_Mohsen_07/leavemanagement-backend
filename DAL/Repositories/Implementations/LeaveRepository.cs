﻿using DAL.Data;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Implementations
{
    public class LeaveRepository : Repository<Leave> , ILeaveRepository
    {
        public LeaveRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
