﻿using DAL.Data;
using DAL.Exceptions;
using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories.Implementations
{
    public class Repository<T> : IRepository<T> where T : class, IBaseModel
    {
        #region Properties

        internal readonly ApplicationDbContext context;
        internal readonly DbSet<T> _dbSet;

        #endregion

        #region Constructors
        public Repository(ApplicationDbContext context)
        {
            this.context = context;
            _dbSet = context.Set<T>();
        }

        #endregion

        #region GET Methods

        public async virtual Task<T> GetAsync(object id)
        {
            var entity = await _dbSet.FindAsync(id);
            if (entity == null)
            {
                throw new DbOperationException(string.Format("No record with primary key Id = {0} exists.", id), new Exception("Repository -> [GetAsync]"));
            }
            return entity;
        }
        public async virtual Task<IList<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                // Apply Eager Loading
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }

            return await query.ToListAsync();
        }
        public async virtual Task<IList<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        #endregion

        #region ADD Methods

        public async virtual Task AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", new DbOperationException("Repository -> [AddAsync]"));
            }

            await _dbSet.AddAsync(entity);
        }

        public async virtual void AddRangeAsync(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities", new DbOperationException("Repository -> [AddRangeAsync]"));
            }

            foreach (var entity in entities)
            {
                await AddAsync(entity);
            }
        }

        #endregion

        #region REMOVE Methods

        public async virtual void RemoveAsync(object id)
        {
            T entityToDelete = await GetAsync(id);
            entityToDelete.IsDeleted = true;
        }

        public virtual void Remove(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", new DbOperationException("Repository -> [Remove]"));
            }

            entity.IsDeleted = true;
        }

        public virtual void RemoveRange(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entities", new DbOperationException("Repository -> [RemoveRange]"));
            }

            foreach (var entity in entities)
            {
                Remove(entity);
            }
        }


        #endregion

        #region UPDATE Methods

        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", new DbOperationException("Repository -> [Update]"));
            }

            _dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        #endregion

        #region QUERIES Methods

        public async virtual Task<IList<T>> WhereAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            return await GetAsync(filter: predicate, orderBy, includeProperties);
        }

        #endregion
    }

}
