﻿using DAL.Models;

namespace DAL.Repositories.Interfaces
{
    public interface IApplicationRoleRepository : IRepository<ApplicationRole>
    {
    }
}