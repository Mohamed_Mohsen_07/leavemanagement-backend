﻿using DAL.Models;

namespace DAL.Repositories.Interfaces
{
    public interface ILeaveReasonRepository : IRepository<LeaveReason>
    {
    }
}
