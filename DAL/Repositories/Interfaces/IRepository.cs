﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        #region GET Methods

        Task<T> GetAsync(object id);
        Task<IList<T>> GetAsync(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");
        Task<IList<T>> GetAllAsync();
        
        #endregion

        #region ADD Methods

        Task AddAsync(T entity);
        void AddRangeAsync(IEnumerable<T> entities);

        #endregion

        #region REMOVE Methods

        void RemoveAsync(object id);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);

        #endregion

        #region UPDATE Methods
        void Update(T entity);

        #endregion

        #region QUERIES Methods

        Task<IList<T>> WhereAsync(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");
        
        #endregion
    }
}
