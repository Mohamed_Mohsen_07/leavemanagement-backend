﻿using DAL.Data;
using DAL.Exceptions;
using DAL.Repositories.Implementations;
using DAL.Repositories.Interfaces;
using System;

namespace DAL.UnitOfWork
{
    public class ApplicationUnitOfWork : IApplicationUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public ILeaveRepository LeaveRepository { get; }
        public ILeaveReasonRepository LeaveReasonRepository { get; }
        public IApplicationUserRepository ApplicationUserRepository { get; }

        private bool _disposed;

        public ApplicationUnitOfWork(ApplicationDbContext context, ILeaveRepository leaveRepository, ILeaveReasonRepository leaveReasonRepository, IApplicationUserRepository applicationUserRepository)
        {
            _context = context;
            LeaveRepository = leaveRepository;
            LeaveReasonRepository = leaveReasonRepository;
            ApplicationUserRepository = applicationUserRepository;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    try
                    {
                        _context.Dispose();
                    }
                    catch (Exception ex)
                    {

                        throw new DbOperationException("An error is encountered while disposing to the database. ApplicationUnitOfWork -> [Dispose]", ex);
                    }
                }

                _disposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Saves all changes made in this context to the database.
        /// </summary>
        /// <returns>The number of state entries written to the database.</returns>
        public int Commit()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new DbOperationException("An error is encountered while commiting to the database. ApplicationUnitOfWork -> [Commit]", ex);
            }
        }
    }
}
