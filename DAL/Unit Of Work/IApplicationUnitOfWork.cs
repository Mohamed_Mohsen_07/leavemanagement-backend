﻿using DAL.Repositories.Implementations;
using DAL.Repositories.Interfaces;
using System;

namespace DAL.UnitOfWork
{
    public interface IApplicationUnitOfWork : IDisposable
    {
        ILeaveRepository LeaveRepository { get; }
        ILeaveReasonRepository LeaveReasonRepository { get; }
        IApplicationUserRepository ApplicationUserRepository { get; }
        int Commit();
    }
}
