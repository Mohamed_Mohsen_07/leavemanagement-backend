﻿namespace ResponseHandler.Enums
{
    public enum Code
    {
        // Not Set Code Responses
        NONE = -1,

        // Success Code Responses
        SuccessRetriveData = 1000,
        SuccessCreateNewUser = 1001,
        SuccessLogin = 1002,
        SuccessCreateNewLeave = 1003,
        SuccessIsLoggedIn = 1004,
        SuccessLogout = 1005,

        // Bad Code Responses
        FailedToRetriveData = 4000,
        IncorrectUserName = 4001,
        IncorrectEmail = 4002,
        IncorrectUserNameOrPassword = 4003,
        IncorrectEmailOrPassword = 4004,
        FailedToCreateNewUser = 4005,
        ModelStateNotValid = 4006,
        FailedIsLoggedIn = 4007,
        FailedToCreateNewLeave = 4007,

        Exception = 7000,
        RegisterException = 7001,
        CreateNewLeaveException = 7002,
        LoginException = 7004,
        IsLoggedInException = 7005,

    }
}
