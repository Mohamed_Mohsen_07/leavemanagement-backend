﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResponseHandler.Enums
{
    public enum CodeType
    {
        Unknown = -1,
        Failure = 400,
        Success = 200,
        Exception = 500
    }
}
