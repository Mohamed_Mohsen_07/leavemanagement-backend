﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResponseHandler.Enums
{
    public enum MessageType
    {
        None = -1,
        Info,
        Warning,
        Error,
        Success,
        Exception
    }
}
