﻿using ResponseHandler.Factories.Interfaces;
using ResponseHandler.MessagesHandler;
using ResponseHandler.Models;
using ResponseHandler.Enums;
using System.Collections.Generic;

namespace ResponseHandler.Factories.Implementations
{
    public class ResponseFactory : IResponseFactory
    {
        public APIResponse Generate(Code code, object result = null, string[] messagesParams = null, ReturnMessage message = null)
        {
            return new APIResponse()
            {
                Code = (int)code,
                Message = message ?? CodeMessagesHandler.GetReturnMessageForCode(code, messagesParams),
                CodeType = CodeMessagesHandler.GetCodeType(code).ToString().ToLower(),
                Result = result
            };
        }

        public APIResponse GenerateAlertMessages(Code code, List<string> alertMessages, object result = null)
        {
            return new APIResponse()
            {
                Code = (int)code,
                Message = CodeMessagesHandler.GetReturnMessageForCode(code, alertMessages),
                CodeType = CodeMessagesHandler.GetCodeType(code).ToString().ToLower(),
                Result = result
            };
        }
    }
}
