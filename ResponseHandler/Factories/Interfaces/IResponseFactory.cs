﻿using ResponseHandler.Models;
using ResponseHandler.Enums;
using System.Collections.Generic;

namespace ResponseHandler.Factories.Interfaces
{
    public interface IResponseFactory
    {
        /// <summary>
        /// Generate an uniformed response for the API.
        /// </summary>
        /// <param name="code">Internal code categorize the response.</param>
        /// <param name="result">The actual return content for the API.</param>
        /// <param name="message">A message to overwrite the default message for the returning code.</param>
        /// <returns>Generated uniformed response.</returns>
        APIResponse Generate(Code code, object result = null, string[] messagesParams = null, ReturnMessage message = null);


        /// <summary>
        /// Generate an uniformed response for the API.
        /// </summary>
        /// <param name="code">Internal code categorize the response.</param>
        /// <param name="alertMessages">Alert messages.</param>
        /// <param name="result">The actual return content for the API.</param>
        /// <returns>Generated uniformed response.</returns>
        APIResponse GenerateAlertMessages(Code code, List<string> alertMessages, object result = null);
    }
}
