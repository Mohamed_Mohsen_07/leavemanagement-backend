﻿using Newtonsoft.Json;
using ResponseHandler.Enums;
using ResponseHandler.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace ResponseHandler.MessagesHandler
{
    internal class CodeMessagesHandler
    {
        private static Dictionary<Code, ReturnMessage> CodeMessages;

        public static bool IsValidCode(Code code)
        {
            return CodeMessages.ContainsKey(code);
        }

        public static ReturnMessage GetReturnMessageForCode(Code code, string[] messagesParams)
        {
            OnBeforeExecute(code);
            var codeMessage = CodeMessages[code];
            if (messagesParams != null)
            {
                for (int i = 0; i < codeMessage.Messages.Count; i++)
                {
                    for (int j = 0; j < messagesParams.Length; j++)
                    {
                        codeMessage.Messages[i] = codeMessage.Messages[i].Replace("{" + j + "}", messagesParams[j]);
                    }
                }
            }
            return codeMessage;
        }

        public static ReturnMessage GetReturnMessageForCode(Code code, List<string> messages)
        {
            OnBeforeExecute(code);
            var codeMessage = CodeMessages[code];
            if (messages != null)
            {
                codeMessage.Messages = messages;
            }
            return codeMessage;
        }

        public static CodeType GetCodeType(Code code)
        {
            int _code = (int)code;

            if (_code >= 1000 && _code < 4000)
                return CodeType.Success;

            if (_code >= 4000 && _code < 7000)
                return CodeType.Failure;

            if (_code >= 7000 && _code < 9000)
                return CodeType.Exception;

            /*
             if (_code >= 9000 && _code < 10000)
                it's a front-end exception.
             */

            return CodeType.Unknown;
        }

        private static void LoadCodeMessages()
        {
            try
            {
                string jsonContent = File.ReadAllText(Constants.MessagesJsonFilePath);
                var loadedCodeMessages = JsonConvert.DeserializeObject<List<CodeMessage>>(jsonContent);
                CodeMessages = new Dictionary<Code, ReturnMessage>();
                foreach (var codeMessage in loadedCodeMessages)
                {
                    if (CodeMessages.ContainsKey(codeMessage.Code))
                    {
                        // LOG FAULT
                        continue;
                    }
                    CodeMessages.Add(codeMessage.Code, codeMessage.ReturnMessage);
                }
            }
            catch (Exception ex)
            {
                // LOG EXCEPTION
                throw ex;
            }

        }

        private static void OnBeforeExecute(Code code)
        {
            if (CodeMessages == null)
            {
                LoadCodeMessages();
            }

            if (!CodeMessages.ContainsKey(code))
            {
                AddInvalidCode(code);
            }
        }

        private static void AddInvalidCode(Code code)
        {
            CodeMessages.Add(code, new ReturnMessage("INVALID CODE", "INVALID CODE", MessageType.None, false));
        }
    }
}
