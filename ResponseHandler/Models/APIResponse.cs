﻿namespace ResponseHandler.Models
{
    public class APIResponse
    {
        public int Code { get; set; }
        public string CodeType { get; set; }
        public ReturnMessage Message { set; get; }
        public object Result { get; set; }
    }
}
