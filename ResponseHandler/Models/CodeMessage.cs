﻿using System;
using System.Collections.Generic;
using System.Text;
using ResponseHandler.Enums;

namespace ResponseHandler.Models
{
    public class CodeMessage
    {
        public Code Code { get; set; }
        public ReturnMessage ReturnMessage { get; set; }
    }
}
