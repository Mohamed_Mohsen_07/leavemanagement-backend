﻿using ResponseHandler.Factories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ResponseHandler.Enums;

namespace ResponseHandler.Models
{
    public class ReturnMessage
    {
        public List<string> Messages { get; set; }
        public string Title { get; set; }
        public string MessageType { get; set; }
        public bool ShowMessage { get; set; }

        public ReturnMessage() { }

        public ReturnMessage(string title, string message, MessageType messageType, bool showMessage)
        {
            Title = title;
            Messages = new List<string>();
            Messages.Add(message);
            MessageType = messageType.ToString().ToLower();
            ShowMessage = showMessage;
        }

        public ReturnMessage(string title, List<string> messages, MessageType messageType, bool showMessage)
        {
            Title = title;
            Messages = messages;
            MessageType = messageType.ToString().ToLower();
            ShowMessage = showMessage;
        }
    }
}
